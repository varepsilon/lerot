import unittest

from lerot.ranker import SyntheticDeterministicRankingFunction
from lerot.document import Document

from lerot.comparison import OptimizedInterleaveVa


class TestEvaluation(unittest.TestCase):

    def setUp(self):
        pass

    def makeSerps(self, templates):
        types = {'w': 'Web', 'i': 'Image', 'n': 'News'}
        serps = []
        mapping = {}
        for template in templates:
            counter = 0
            serp = []
            for d in template:
                while counter in mapping and mapping[counter] != d:
                    counter += 1
                mapping[counter] = d
                counter += 1
                serp.append(Document(counter, types[d]))

            serps.append(serp)
        return serps

    def testVA(self):
        #strA = 'wwwnnwwiiiwww'
        #strB = 'wnnnwiiwwwwww'

        #strA = 'wwwwwwwiiiwnw'
        #strB = 'wwwwwwwiiiwwn'

        strA = 'wni'
        strB = 'win'

        dA, dB = self.makeSerps([strA, strB])

        #Web, News = 'Web', 'News'
        #dA, dB = [Document(1, Web), Document(0, Web), Document(5, Web), Document(9, Web), Document(4, Web), Document(7, Web), Document(10, Web), Document(2, News), Document(3, News), Document(6, News)], [Document(0, Web), Document(1, Web), Document(4, Web), Document(5, Web), Document(9, Web), Document(7, Web), Document(2, News), Document(3, News), Document(6, News), Document(8, News)]

        comparison = OptimizedInterleaveVa('')
        A = SyntheticDeterministicRankingFunction(dA)
        B = SyntheticDeterministicRankingFunction(dB)
        dI, a = comparison.interleave(A, B, None, min(len(dA), len(dB)))
        print dA
        print dB
        print list(dI)


if __name__ == '__main__':
    # from pycallgraph import PyCallGraph
    # from pycallgraph.output import GraphvizOutput
    #
    # with PyCallGraph(output=GraphvizOutput()):
    unittest.main()
